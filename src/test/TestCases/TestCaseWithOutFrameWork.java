import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestCaseWithOutFrameWork {
    @Test
    public void setBaseURL() {
        //Chrome Browser Location
        String path = "./drivers/chromedriver.exe";
        String driverName = "webdriver.chrome.driver";
        System.setProperty(driverName, path);
        WebDriver driver = new ChromeDriver();

        //open the browser on the second screen
        driver.manage().window().setPosition(new Point(3000, 1));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.daka90.co.il");

        driver.findElement(By.xpath("//*[@id='buttontab5']")).click();
        //WaitForElement.waitForElementToBeclickable(driver, categoryButtonElement, 10).click();
    }
}
