

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class LoginTest extends BaseClass{

    @Test
    public void LoginApp() {

        ExcelDataProvider excel=new ExcelDataProvider();

        LoginPage loginPage= PageFactory.initElements(driver, LoginPage.class);

        loginPage.LoginToAccount(excel.getStringData("Login",0,0),excel.getStringData("Login",0,1));

        //screenshot when the test pass
        Helper.captureScreenshot(driver);

    }



}
