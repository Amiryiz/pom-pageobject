//package Utils;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BrowserFactory {
    public static WebDriver startApplication(WebDriver driver, String browserName, String appURL)
    {
        if(browserName.equals("Chrome"))
        {

            System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
            driver=new ChromeDriver();
        }
        else if(browserName.equals("FireFox"))
        {
            System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
            driver=new ChromeDriver();
        }

        else if(browserName.equals("IE"))
        {
            System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer_32.exe");
            driver=new ChromeDriver();
        }
        else
        {
            System.out.println("We don't support this browser");
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

//open the browser on the second screen
        driver.manage().window().setPosition(new Point(3000, 1));
        driver.manage().window().maximize();
        driver.get(appURL);

        return driver;

    }

    public static void quitApplication(WebDriver driver)
    {
        driver.quit();
    }


}
